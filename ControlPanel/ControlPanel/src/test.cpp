#include "inc/test.h"

Test::Test(ControlPanel *controller, MqttClient *mqtt, QObject *parent)
  : QObject(parent)
  , controller(controller)
  , mqtt(mqtt)
{
  loadAvailableRobots();

  connect(controller->view->ui->btnRefreshRobots, SIGNAL(released()), this, SLOT(on_btnRefreshRobots_release()));
  connect(controller->view->ui->btnSendCurveControl, SIGNAL(released()), this, SLOT(on_btnSendCurveControl_release()));
  connect(controller->view->ui->btnSendCustomText, SIGNAL(released()), this, SLOT(on_btnSendCustomText_release()));
}


void Test::loadAvailableRobots()
{
  if(mqtt != nullptr)
  {
    std::vector<int> registeredIds = mqtt->getRegisteredRobotIds();
    foreach(int id, registeredIds)
    {
        controller->view->ui->robotSelector->addItem(QString::number(id));
    }
  }
}

void Test::setMqttClient(MqttClient *mqtt)
{
  this->mqtt = mqtt;
}




/*****************
 * SLOTS
 */

void Test::on_btnRefreshRobots_release()
{
  loadAvailableRobots();
}

void Test::on_btnSendCurveControl_release()
{
  if(mqtt != nullptr){
    Robot* robot = mqtt->getRobot(controller->view->ui->robotSelector->currentText().toInt());
    if(robot)
    {
        float R_value;
        float L_value;
        bool isOk;
        R_value = controller->view->ui->txtRValue->text().toFloat(&isOk);
        if(isOk)
        {
          L_value = controller->view->ui->txtLValue->text().toFloat(&isOk);
          if(isOk)
          {
            controller->writeToConsole(QString("R: ").append(QString::number(R_value)));
            controller->writeToConsole(QString("L: ").append(QString::number(L_value)));
            robot->goCurve(R_value, L_value);
          }
          else
          {
            controller->writeToConsole("Invalid L value", ControlPanel::red);
          }
        }
        else
        {
          controller->writeToConsole(QString("Invalid R value..."), ControlPanel::red);
        }
    }
    else
    {
      controller->writeToConsole(
            QString("There is no registered robot with id ")
            .append(QString::number(controller->view->ui->robotSelector->currentText().toInt()))
            , ControlPanel::red);
    }
  }
  else
  {
      controller->writeToConsole(
            "Mqtt API disconnected"
            , ControlPanel::red);
  }
}

void Test::on_btnSendCustomText_release()
{
  if(mqtt != nullptr)
  {
    QString subtopic = controller->view->ui->txtSubtopic->text();
    QString message = controller->view->ui->txtCustomMessage->toPlainText();

    Robot* robot = mqtt->getRobot(controller->view->ui->robotSelector->currentText().toInt());
    if(robot)
    {
        robot->sendMessage(message, subtopic);
    }
    else
    {
      controller->writeToConsole(
            QString("There is no registered robot with id ")
            .append(QString::number(controller->view->ui->robotSelector->currentText().toInt()))
            , ControlPanel::red);
    }
  }
  else
  {
      controller->writeToConsole(
            "Mqtt API disconnected"
            , ControlPanel::red);
  }
}
