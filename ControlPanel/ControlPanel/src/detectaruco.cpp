﻿#include "detectaruco.h"
#include <iostream>
#include <opencv2/aruco.hpp>
#include <opencv2/opencv.hpp>
#include <math.h>
#include "mainwindow.h"
#include <QDebug>

using namespace std;
using namespace cv;

cv::Point2f o1, p1;

MarkerDetector::MarkerDetector(std::string camera_parameters_filename, int dictId, float marker_size, std::string dictionary_filename)
  : camera_parameters_filename(camera_parameters_filename)
  , dictId(dictId)
  , marker_size(marker_size)
  , dictionary_filename(dictionary_filename)
  , estimatePose(false)
{
  bool readOk = readCameraParameters(camera_parameters_filename, camMatrix, distCoeffs);
  if(readOk){
      estimatePose = true;
  }
  if(dictionary_filename!=""){
      //using custom dictionary from file
      cv::FileStorage fsr(dictionary_filename, cv::FileStorage::READ);
      int mCBits;
      cv::Mat bits;
      fsr["MarkerSize"] >> marker_size;
      fsr["MaxCorrectionBits"] >> mCBits;
      fsr["ByteList"] >> bits;
      fsr.release();
      dict3x3 = cv::aruco::Dictionary(bits, marker_size, mCBits);
      dictionary=&dict3x3;
  } else {
      //using predefined dictionary by ID
      dictionary = aruco::getPredefinedDictionary(aruco::PREDEFINED_DICTIONARY_NAME(dictId));
  }
  detectorParams = aruco::DetectorParameters::create();
}

void MarkerDetector::setRefresh(){
  refresh = true;
}

bool MarkerDetector::readCameraParameters(string filename, Mat &camMatrix, Mat &distCoeffs) {
  FileStorage fs(filename, FileStorage::READ);
  if(!fs.isOpened())
      return false;
  fs["camera_matrix"] >> camMatrix;
  fs["distortion_coefficients"] >> distCoeffs;
  return true;
}


cv::Mat MarkerDetector::detectMarker(cv::Mat frame){
  Mat imageCopy;
  // detect markers and estimate pose
  aruco::detectMarkers(frame, dictionary, corners, ids, detectorParams, rejected);
  if(estimatePose && ids.size() > 0)
      aruco::estimatePoseSingleMarkers(corners, marker_size, camMatrix, distCoeffs, rvecs,
                                       tvecs);
  //teszt
  if(refresh){
    gui->ui->idsBox->clear();
    QStringList ids_text = QStringList();
    for(auto id : ids){
      ids_text << QString::number(id);
    }
    gui->ui->idsBox->addItems(ids_text);
  }
  /*if(gui->ui->idsBox->count() > 0){
    auto id_it = find(ids.begin(), ids.end(), gui->ui->idsBox->currentText().toInt());
    if(id_it != ids.end()){
      int pos = distance(ids.begin(), id_it);
      tvecs[pos][0] += gui->ui->XSlider->value();
      tvecs[pos][1] += gui->ui->YSlider->value();
      tvecs[pos][2] += gui->ui->ZSlider->value();
    }
    refresh = false;
  }*/

  //teszt end

  frame.copyTo(imageCopy);

  // draw results
  //frame.copyTo(imageCopy);
  if(ids.size() > 0) {
      aruco::drawDetectedMarkers(imageCopy, corners, ids);

      if(estimatePose) {
          imgPointsVector.clear();
          for(unsigned int i = 0; i < ids.size(); i++){
              /*aruco::drawAxis(imageCopy, camMatrix, distCoeffs, rvecs[i], tvecs[i],
                              marker_size * 0.5f);*/

              float length = marker_size * 0.5f;
              vector< Point3f > axisPoints;
              float x = 0, y = 0, z = 0;
              if(gui->ui->idsBox->count() > 0 && ids[i] == gui->ui->idsBox->currentText().toInt()){

                x = gui->ui->XSlider->value();
                y = gui->ui->YSlider->value();
                z = gui->ui->ZSlider->value();

                refresh = false;
              }

              if(ids[i] == gui->ui->robotIDIn->value() && gui->ui->robotIDIn->value()==11){  //koala

                x = 0;
                y = 0;
                z = -0.23;

                refresh = false;
              }

              if(ids[i] == gui->ui->robotIDIn->value() && gui->ui->robotIDIn->value()==12){  //rc

                x = 0;
                y = 0;
                z = -0.24;

                refresh = false;
              }

              axisPoints.push_back(Point3f(x, y, z));
              axisPoints.push_back(Point3f(length + x, y, z));
              axisPoints.push_back(Point3f(x, length + y, z));
              axisPoints.push_back(Point3f(x, y, length + z));



              //vector< Point2f > imagePoints;
              cv::projectPoints(axisPoints, rvecs[i], tvecs[i], camMatrix, distCoeffs, imagePoints);

              // draw axis lines
              cv::line(imageCopy, imagePoints[0], imagePoints[1], Scalar(0, 0, 255), 3);
              cv::line(imageCopy, imagePoints[0], imagePoints[2], Scalar(0, 255, 0), 3);
              cv::line(imageCopy, imagePoints[0], imagePoints[3], Scalar(255, 0, 0), 3);
              imgPointsVector.push_back(imagePoints);
          }
      }
  }
  return imageCopy;
}

void MarkerDetector::setImageTransformator(ImgTransform* imgT){
  this->imgT = imgT;
}

void MarkerDetector::setGui(MainWindow *gui){
  this->gui = gui;
}

vector<int> MarkerDetector::getIds(){
  return ids;
}

vector< vector< Point2f > > MarkerDetector::getCorners(){
  return corners;
}

bool MarkerDetector::getRVecsOf(int markerId, cv::Vec3d& rvecs){
    bool isFound = false;
    auto it = find(ids.begin(), ids.end(), markerId);
    if(it != ids.end()){
      int position = distance(ids.begin(), it);
      rvecs = this->rvecs.at(position);
      isFound = true;
    }

    return isFound;
}

bool MarkerDetector::getTVecsOf(int markerId, cv::Vec3d& tvecs){
    bool isFound = false;
    auto it = find(ids.begin(), ids.end(), markerId);
    if(it != ids.end()){
      int position = distance(ids.begin(), it);
      tvecs = this->tvecs.at(position);
      isFound = true;
    }

    return isFound;
}


bool MarkerDetector::getCornersOf(int markerId, std::vector< cv::Point2f >& corners){
  bool isFound = false;
  auto it = find(ids.begin(), ids.end(), markerId);
  if(it != ids.end()){
    int position = distance(ids.begin(), it);
    corners = this->corners.at(position);
    Point2f oldCenter, newCenter, vector ;
    std::vector<cv::Point2f> transCorners;
    if(markerId==gui->ui->robotIDIn->value()){
        if (getCOGCoordinatesOf(gui->ui->robotIDIn->value(), oldCenter) && getCenterOf(gui->ui->robotIDIn->value(),newCenter)){
            vector=Point2f(newCenter.x-oldCenter.x, newCenter.y-oldCenter.y);
            foreach (Point2f p, corners){
                p.x= p.x+vector.x;
                p.y= p.y+vector.y;
                transCorners.push_back(p);
             }
            corners=transCorners;
        }
    }

    isFound = true;
  }

  return isFound;
}

bool MarkerDetector::getCOGCoordinatesOf(int markerId, cv::Point2f& coord){
  bool isFound = false;
  auto it = find(ids.begin(), ids.end(), markerId);
  if(it != ids.end()){
    int position = distance(ids.begin(), it);
    //compute cog
    vector< Point3f > cogPoints = { Point3f(0,0,0) };
    vector< Point2f > imagePoints;
    cv::projectPoints(cogPoints, rvecs[position], tvecs[position], camMatrix, distCoeffs, imagePoints);
    coord = imagePoints[0];
    isFound = true;
  }

  return isFound;
}

bool MarkerDetector::getCenterOf(int markerId, cv::Point2f& center){
    bool isFound = false;
    auto it = find(ids.begin(), ids.end(), markerId);
    if(it != ids.end()){
      vector< Point2f > imagePts;
      int position = distance(ids.begin(), it);
      imagePts = this->imgPointsVector.at(position);
      center=imagePts[0];
      isFound = true;
    }

    return isFound;
}


cv::Mat MarkerDetector::getCameraMatrix()
{
  return camMatrix;
}

cv::Mat MarkerDetector::getDistCoeffs()
{
  return distCoeffs;
}

vector<Point3f> getCornersInCameraWorld(double side, Vec3d rvec, Vec3d tvec){

     double half_side = side/2;

     // compute rot_mat
     Mat rot_mat;
     Rodrigues(rvec, rot_mat);

     // transpose of rot_mat for easy columns extraction
     Mat rot_mat_t = rot_mat.t();

     // the two E-O and F-O vectors
     double * tmp = rot_mat_t.ptr<double>(0);
     Point3f camWorldE(tmp[0]*half_side,
                       tmp[1]*half_side,
                       tmp[2]*half_side);

     tmp = rot_mat_t.ptr<double>(1);
     Point3f camWorldF(tmp[0]*half_side,
                       tmp[1]*half_side,
                       tmp[2]*half_side);

     // convert tvec to point
     Point3f tvec_3f(tvec[0], tvec[1], tvec[2]);

     // return vector:
     vector<Point3f> ret(4,tvec_3f);

     ret[0] +=  camWorldE + camWorldF;
     ret[1] += -camWorldE + camWorldF;
     ret[2] += -camWorldE - camWorldF;
     ret[3] +=  camWorldE - camWorldF;

     return ret;
}

vector<Point3f> MarkerDetector::getCornersOfInCameraWorld(double side, int markerId){
    Vec3d rvec, tvec;
    auto it = find(ids.begin(), ids.end(), markerId);
    if(it != ids.end()){
      int position = distance(ids.begin(), it);
      rvec = this->rvecs.at(position);
      tvec = this->tvecs.at(position);
    }

     double half_side = side/2;

     // compute rot_mat
     Mat rot_mat;
     Rodrigues(rvec, rot_mat);

     // transpose of rot_mat for easy columns extraction
     Mat rot_mat_t = rot_mat.t();

     // the two E-O and F-O vectors
     double * tmp = rot_mat_t.ptr<double>(0);
     Point3f camWorldE(tmp[0]*half_side,
                       tmp[1]*half_side,
                       tmp[2]*half_side);

     tmp = rot_mat_t.ptr<double>(1);
     Point3f camWorldF(tmp[0]*half_side,
                       tmp[1]*half_side,
                       tmp[2]*half_side);

     // convert tvec to point
     Point3f tvec_3f(tvec[0], tvec[1], tvec[2]);

     // return vector:
     vector<Point3f> ret(4,tvec_3f);

     ret[0] = ret[0]+  camWorldE + camWorldF;
     ret[1] = ret[1]-camWorldE + camWorldF;
     ret[2] = ret[2]-camWorldE - camWorldF;
     ret[3] = ret[3]+ camWorldE - camWorldF;

     return ret;
}
