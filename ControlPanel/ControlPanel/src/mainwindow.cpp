﻿#include "inc/mainwindow.h"

#include <opencv2/opencv.hpp>
#include <iostream>
#include <QCameraInfo>
#include <QList>
#include <QFileDialog>
#include "detectaruco.h"

using namespace cv;

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  loadCameras();
}

MainWindow::~MainWindow()
{
  delete ui;
}



void MainWindow::loadCameras(){
  ui->cameraSelector->clear();
  QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
  QStringList list_of_cameras = QStringList();
  foreach (const QCameraInfo &cameraInfo, cameras) {
      list_of_cameras << cameraInfo.description();
      auto m_camera = new QCamera(cameraInfo, this);
      m_camera->load();
      cameras_resolution.push_back(m_camera->supportedViewfinderResolutions());
      delete m_camera;
  }
  ui->cameraSelector->addItems(list_of_cameras);
  if(!cameras_resolution.empty())
    loadResolutions(cameras_resolution[0]);

}

void MainWindow::loadResolutions(QList<QSize> resolutions){
  QStringList res_list;
  foreach(const QSize &res, resolutions){
    res_list.append(QString::number(res.width()).append("x").append(QString::number(res.height())));
  }
  ui->resolutionSelector->clear();
  ui->resolutionSelector->addItems(res_list);
}

void MainWindow::writeToConsole(QString text, QString color){

  QString format = "<span style=\"color:%1\">%2<span>";
  ui->uiConsole->append(format.arg(color).arg(text));
}



bool MainWindow:: getIsCheckedShowPerspective(){
  return ui->chckShowPerspTrans->isChecked();
}

QPushButton* MainWindow::getBtnOpenCamera(){
  return ui->btnOpenCamera;
}
