#include "inc/mqttapi.h"
#include "inc/robot.h"
#include <QDebug>
#include <vector>
/*******************************************************************************************
 *                                                                                         *
 *                                  MqttAPI implementation                                 *
 *                                                                                         *
 *******************************************************************************************/



MqttClient::MqttClient(QString host, quint16 port, ControlPanel *controller, bool debug, QObject *parent) : QObject(parent){
  this->host = host;
  this->port = port;
  this->controller = controller;
  this->debug = debug;
  this->validator = Validator();

  this->client = new QMqttClient(this);
  this->client->setHostname(host);
  this->client->setPort(port);

  connect(client, SIGNAL(stateChanged(ClientState)), this, SLOT(event_stateChanged()));
  connect(client, SIGNAL(messageReceived(QByteArray,QMqttTopicName)), this, SLOT(event_messageReceived(const QByteArray&, const QMqttTopicName&)));
  connect(client, SIGNAL(connected()), this, SLOT(event_connected()));
  connect(client, SIGNAL(disconnected()), this, SLOT(event_disconnected()));
    
}

MqttClient::~MqttClient(){
    delete client;
    deleteRegisteredRobots();
      
}

void MqttClient::setDebugMode(bool isEnabled){
  this->debug = isEnabled;
}

void MqttClient::connectToHost(){
  if(debug)
    controller->writeToConsole(QString(API_NAME).append(": Trying to connect to MQTT broker: ").append(host).append(":").append(QString::number(port)), INFO);

  client->connectToHost();
}

void MqttClient::disconnect(){
  client->disconnectFromHost();
}

void MqttClient::event_stateChanged(){
  QString state;
  switch(client->state()){
    case 0:
      state = "Disconnected";
      break;
    case 1:
      state = "Connecting";
      break;
    case 2:
      state = "Connected";
      break;
  }

  const QString content = QString(API_NAME)
                  + QLatin1String(": State Change - ")
                  + state
                  + QLatin1Char('\n');
  controller->writeToConsole(content, INFO);
}

void MqttClient::event_connected(){
  controller->setEnableOrDisableMqttConnectionFields(false);
  //subscribe to controller's topic
  subscribeTo(CONTROLLER_TOPIC_NAME);
}

void MqttClient::event_disconnected(){
  controller->setEnableOrDisableMqttConnectionFields(true);
  deleteRegisteredRobots();
}

void MqttClient::event_messageReceived(const QByteArray &message, const QMqttTopicName &topic){
  if(debug){
    controller->writeToConsole(QString(API_NAME).append(": [" + QDateTime::currentDateTime().time().toString() + "]\n"), INFO);
    controller->writeToConsole(QString("   Received Topic: <b>").append(topic.name() + "<b>\n"), INFO);
    controller->writeToConsole(QString("   Message: <b>").append(message + "<b>\n"), INFO);
  }
  messageParser(message);
}

void MqttClient::subscribeTo(QString topic){
  auto subscription = client->subscribe(topic);
  if(!subscription){
    controller->writeToConsole(QString("Failed to subscript: ").append(topic), ERROR);
  }else{
    if(debug){
      QString msg = API_NAME;
      msg.append(": Subscribed to topic: ").append(topic);
      controller->writeToConsole(msg, INFO);
    }
    subscriptions.insert(subscription);
  }
}

bool MqttClient::sendMessage(QString topic, QString msg){
  bool isSuccess = true;
  if(client->publish(QMqttTopicName(topic), QByteArray(msg.toStdString().c_str())) == -1){
    QString content = "Failed to send: " + topic;
    controller->writeToConsole(content, ERROR);
    isSuccess = false;
  }else{
    if(debug){
      QString content = API_NAME;
      content += ": Message sent to \"" + topic + "\"<br/>";
      content += "Message:  <b>" + msg + "</b>";
      controller->writeToConsole(content, INFO);

    }
  }
  return isSuccess;
}

Robot* MqttClient::getRobot(unsigned id)
{
  foreach(Robot* robot, registeredRobots)
  {
    if(robot->getId() == id)
    {
      return robot;
    }
  }

  return nullptr;
}

std::vector<int> MqttClient::getRegisteredRobotIds()
{
  std::vector<int> registeredIds;
  foreach(Robot* robot, registeredRobots)
  {
    registeredIds.push_back(robot->getId());
  }
  return registeredIds;
}


//https://doc.qt.io/QtMQTT/qtmqtt-simpleclient-example.html

//{ "cmd":"reg", "id":5, "topic":"robot1" }

void MqttClient::dumpErrors(std::vector<QString> &errors){
  if(debug){
    for(auto error : errors){
      controller->writeToConsole(error, ERROR);
    }
  }
}

void MqttClient::messageParser(QString msg){
  QJsonDocument doc = QJsonDocument::fromJson(msg.toUtf8());
  QJsonObject obj;
  if(!doc.isNull())
  {
    if(doc.isObject())
    {
      obj = doc.object();
      commandHandler(obj);
    }
    else
    {
      if(debug)
        controller->writeToConsole("Document is not an object", ERROR);
    }
  }
  else
  {
    if(debug)
      controller->writeToConsole("Invalid JSON...\n", ERROR);
  }

}

void MqttClient::commandHandler(QJsonObject& cmd){
  std::vector<QString> errors;

  if(cmd.contains("cmd") && cmd.value("cmd").isString())
  {
    QString command = cmd.value("cmd").toString();

    if(command == "reg"){
      if(validator.validateReg(cmd,errors)){
        registerRobot((unsigned)cmd.value("id").toInt(), cmd.value("topic").toString());
      }else{
        dumpErrors(errors);
      }
    }else{
      if(debug)
        controller->writeToConsole("Invalid command: " + command, ERROR);
    }
  }else{
    if(debug)
      controller->writeToConsole("Received message does not contain \"cmd\" value or not string.", ERROR);
  }

}

bool MqttClient::registerRobot(unsigned id, QString topic){
  registeredRobots.append(new Robot(id, topic, this));
  //registeredRobots.back()->sendMessage("Successfully registered!");
  return true;
}

void MqttClient::deleteRegisteredRobots(){
    for(int i = 0; i < registeredRobots.size(); ++i){
      delete registeredRobots.at(i);
    }
}



//Validator functions

bool Validator::validateReg(QJsonObject obj, std::vector<QString>& errors){
  ///*********************///
  /// id    : int
  /// topic : string
  ///*********************///
  bool isValid = true;
  if(obj.contains("id")){
    if(!obj.value("id").isDouble()){
      errors.push_back("<b>reg validator: </b>\"id\" is not a number");
      isValid = false;
    }
  }else{
    errors.push_back("<b>reg validator: </b>Key not found: id");
    isValid = false;
  }

  if(obj.contains("topic")){
    if(!obj.value("topic").isString()){
      errors.push_back("<b>reg validator: </b>\"topic\" is not a string");
      isValid = false;
    }
  }else{
    errors.push_back("<b>reg validator: </b>Key not found: topic");
    isValid = false;
  }

  return isValid;
}
