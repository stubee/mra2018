#include "mainwindow.h"
#include "controlpanel.h"
#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  MainWindow w;
  ControlPanel cp(&w);

  return a.exec();
}
