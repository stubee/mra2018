﻿#include "inc/controlpanel.h"
#include "inc/mainwindow.h"
#include "detectaruco.h"
#include "mqttapi.h"
#include "imgtransform.h"

#include <opencv2/opencv.hpp>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>



using namespace cv;
using namespace std;

static cv::Point2f o1, p1, o2, p2, c_org;
static float c_rad;

ControlPanel::ControlPanel(MainWindow *view, QObject *parent) :
  QObject(parent)
, view(view)
, imgT(nullptr)
, md(nullptr)
, server(nullptr)
, mqttClient(nullptr)
{
  view->show();
  writeToConsole("Application started...");
  //connect signals and slots
  connect(view->ui->btnOpenCamera, SIGNAL(released()), this, SLOT(on_btnOpenCamera_released()));
  connect(view->ui->chckArucoDetect, SIGNAL(pressed()), this, SLOT(on_chckArucoDetect_pressed()));
  connect(view->ui->chckShowPerspTrans, SIGNAL(pressed()), this, SLOT(on_chckShowPerspTrans_pressed()));
  connect(view->ui->btnClearConsole, SIGNAL(pressed()), this, SLOT(on_btnClearConsole_released()));
  connect(view->ui->cameraSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(on_cameraSelector_currentIndexChanged(int)));
  connect(view->ui->btnStartServer, SIGNAL(released()), this, SLOT(on_btnStartServer_released()));
  connect(view->ui->btnPaneCalibration, SIGNAL(released()), this, SLOT(on_btnPaneCalibration_released()));
  connect(view->ui->chckMqttDebugEnable, SIGNAL(toggled(bool)), this, SLOT(on_chckMqttDebugEnable_toggled(bool)));
  connect(view->ui->btnRefreshIds, SIGNAL(released()), this, SLOT(on_btnRefreshIds_released()));
  connect(view->ui->btnUpdatePane, SIGNAL(released()), this, SLOT(on_btnUpdatePane_relased()));
  connect(view->ui->btnArucoDict, SIGNAL(released()), this, SLOT(on_btnTargetSetting_released()));

  connect(view->ui->btnMqttConnect, SIGNAL(released()), this, SLOT(on_btnMqttConnect_released()));
  connect(view->ui->btnMqttSubscribe, SIGNAL(released()), this, SLOT(on_btnMqttSubscribe_released()));

  connect(view->ui->btnTestRL, SIGNAL(released()), this, SLOT(on_btnTestRL_released()));

  
  updatePaneSettings();

  test = new Test(this, mqttClient, this);
}

ControlPanel::~ControlPanel(){
  if(imgT != nullptr)
      delete imgT;
  if(server != nullptr){
    delete server;
  }
  if(md != nullptr){
      delete md;
  }

  if(mqttClient != nullptr){
    delete mqttClient;
  }

  if(test != nullptr)
    delete test;
}

void ControlPanel::on_btnOpenCamera_released(){
  //calibration file selector for camera
  if(calib_filename.isEmpty()){
    calib_filename = QFileDialog::getOpenFileName(view,
            tr("Open calibration file"), "",
            tr("All Files (*)"));

  }
  if(calib_filename == "")
      return;

  writeToConsole(QString("Calibration file loaded from: ").append(calib_filename));
  //disable camera selector
  view->ui->cameraSelector->setEnabled(false);
  view->ui->resolutionSelector->setEnabled(false);
  view->ui->btnOpenCamera->setEnabled(false);

  //create detector object
  imgT = new ImgTransform(this);
  
  //read settings value
  float markerSize;
  int dictId;

  if(view->ui->markerDetectorTypeSelector->currentIndex()==1){
    writeToConsole("Dictionary loaded from file.", blue);
    dictionary_filename = QFileDialog::getOpenFileName(view,
                                                       tr("Open dictionary file"), "",
                                                       tr("Dictionary Files (*.yml)"));
  }
  else{
      bool isOk = false;
      markerSize = view->ui->txtMarkerSize->text().toFloat(&isOk);
      if(!isOk){
          writeToConsole("Invalid marker size. Using default value: 0.1", red);
          markerSize = 0.1;
      }
      dictId = view->ui->txtDictId->text().toInt(&isOk);
      if(!isOk){
          writeToConsole("Invalid dictionary ID. Using default: 4", red);
          dictId = 4;
      }
      dictionary_filename="";
  }
  md = new MarkerDetector(calib_filename.toStdString(), dictId, markerSize, dictionary_filename.toStdString());
  md->setImageTransformator(imgT);
  md->setGui(view);

  writeToConsole("Starting stream...");
  startVideoStream(view->ui->cameraSelector->currentIndex(), md);

  md = nullptr;
  //enable camera selector
  view->ui->cameraSelector->setEnabled(true);
  view->ui->resolutionSelector->setEnabled(true);
  view->ui->btnOpenCamera->setEnabled(true);
}


void ControlPanel::startVideoStream(int camera_index, MarkerDetector* md){

    VideoCapture cap(camera_index); // open the default camera
    if(!cap.isOpened())  // check ifupdatePaneSettings we succeeded
    {
      writeToConsole("Failed to open camera...", red );
      return;
    }
    //set resolution
    if(!view->cameras_resolution.empty() && !view->cameras_resolution.at(view->ui->cameraSelector->currentIndex()).empty())
    {
        cap.set(CV_CAP_PROP_FRAME_WIDTH, view->cameras_resolution.at(view->ui->cameraSelector->currentIndex()).at(view->ui->resolutionSelector->currentIndex()).width());
        cap.set(CV_CAP_PROP_FRAME_HEIGHT, view->cameras_resolution.at(view->ui->cameraSelector->currentIndex()).at(view->ui->resolutionSelector->currentIndex()).height());

        /*cap.set(CV_CAP_PROP_FRAME_WIDTH, 800);
        cap.set(CV_CAP_PROP_FRAME_HEIGHT, 600);*/
    }
    
    /*
    cap.set(CV_CAP_PROP_FRAME_WIDTH, 1024);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, 768);
    */
    namedWindow("Video stream",1);
    std::vector<Point> points;
    isStreamOn = true;
    updatePaneSettings();
    for(;;)
    {
      Mat frame;

      cap >> frame; // get a new frame from camera

      /*if (points.size() == 3){
           drawRectangle(points, frame); //target drawing when user selects it
      }*/

      if(md && detectMarkers){
        //remove distortion
        cv::Mat undistortImg;
        cv::undistort(frame, undistortImg, md->getCameraMatrix(), md->getDistCoeffs());
        undistortImg.copyTo(frame);
        frame = md->detectMarker(frame);
      }

      if(aruco_gen){
          aruco_gen=false;
          findTarget(md);
          //creating a new, custom dictionary
          /*int number= 40, dimension=3;
          cv::aruco::Dictionary dictionary = cv::aruco::generateCustomDictionary(number, dimension);
          cv::Mat store=dictionary.bytesList;
          cv::FileStorage fs("dic_3x3_new.yml", cv::FileStorage::WRITE);
          fs << "MarkerSize" << dictionary.markerSize;
          fs << "MaxCorrectionBits" << dictionary.maxCorrectionBits;
          fs << "ByteList" << dictionary.bytesList;
          fs.release();*/

          //opening a custom dictionary
          /*string filename;
          const char* c;
          cv::FileStorage fsr("dic_3x3_new.yml", cv::FileStorage::READ);
          int mSize, mCBits;
          cv::Mat bits;
          fsr["MarkerSize"] >> mSize;
          fsr["MaxCorrectionBits"] >> mCBits;
          fsr["ByteList"] >> bits;
          fsr.release();
          cv::aruco::Dictionary dictionary = cv::aruco::Dictionary(bits, mSize, mCBits);
          cv::Ptr<cv::aruco::Dictionary> dict = &dictionary;
          cv::Mat markerImage;     
            cv::Mat markerImage2;
          //save to file marker
            for(int id=0; id<40;id++){
                cv::aruco::drawMarker(dict, id, 200, markerImage, 1);
                filename="C:/ArucoImages/img_" + to_string(id)+".png";
                c = filename.c_str();
                imwrite(c, markerImage );
                markerImage.release();
            }*/
      }
      if(md && recalibratePane){
          if(calibratePane(md)){
              writeToConsole("Pane calibration successed.");
              calibrated = true;
          }else{
            writeToConsole("Pane calibration failed", red);
            recalibratePane = false;
            calibrated = false;
          }
      }
      
      
      if(paneCorners.size() > 3 && view->getIsCheckedShowPerspective()){
          Mat vertical = this->imgT->getVerticalView(paneCorners, frame);
          drawLine(vertical, o1, p1);
          drawLine(vertical, o2, p2);
          drawLine(vertical, o1, o2);
          cv::circle(vertical, c_org, 3, Scalar(0, 255, 0), 3);
          cv::circle(vertical, c_org, c_rad, Scalar(0, 255, 255), 3);
          std::vector<Point2f> corners;
          if(md->getCornersOf(11, corners)){
              cv::circle(vertical, transformCoordinates(corners[0]), 3, Scalar(255, 0, 0), 3);
              cv::circle(vertical, transformCoordinates(corners[1]), 3, Scalar(255, 0, 0), 3);
              cv::circle(vertical, transformCoordinates(corners[2]), 3, Scalar(255, 0, 0), 3);
              cv::circle(vertical, transformCoordinates(corners[3]), 3, Scalar(255, 0, 0), 3);
          }
          imshow("Result", vertical);
      }

      if(view->ui->chckShowPath->isChecked() && view->ui->idsBox->count() > 0){
//      if(view->ui->idsBox->count() > 0){
        if(this->markerPath.size() > PATH_LENGTH)
          markerPath.pop_back();

        Point2f coord;
        md->getCOGCoordinatesOf(view->ui->idsBox->currentText().toInt(), coord);
        markerPath.push_front(coord);
        drawPath(frame);
      }
      
      imshow("Video stream",frame);
      if(waitKey(30) >= 0) break;
    }
    cvDestroyWindow("Video stream");
    isStreamOn = false;

}



void ControlPanel::on_chckArucoDetect_pressed(){
  if(view->ui->chckArucoDetect->isChecked()){
      detectMarkers = false;
      writeToConsole("Marker detection disabled.", blue);
  }else{
      detectMarkers = true;
      writeToConsole("Marker detection enabled.", blue);
  }
}

void ControlPanel::on_chckShowPerspTrans_pressed(){
  if(!view->ui->chckShowPerspTrans->isChecked()){
    writeToConsole("Displaying perspective transformation is enabled.", blue);
  }else{
    writeToConsole("Displaying perspective transformation is disabled.", blue);
  }
}

void ControlPanel::on_btnClearConsole_released(){
  view->ui->uiConsole->clear();
}

void ControlPanel::on_cameraSelector_currentIndexChanged(int index){
  calib_filename = "";
  view->loadResolutions(view->cameras_resolution.at(index));
}

void ControlPanel::on_btnStartServer_released(){
  if(view->ui->btnStartServer->text() == "Start server"){
    int port;
    QString port_str = view->ui->txtPortNumber->text();
    if(port_str.size() == 0){
      QMessageBox messageBox;
      messageBox.critical(0, "Error", "Port number is empty!");
      return;
    }else{
      QRegExp re("\\d*");
      if(re.exactMatch(port_str)){
          //everything is ok, start the server
          port = port_str.toInt();
          server = new SocketAPI(port, this, view->ui->chckDebugEnable->isChecked());

          view->ui->btnStartServer->setText("Stop server");
          view->ui->txtPortNumber->setEnabled(false);
          view->ui->chckDebugEnable->setEnabled(false);

        }else{
          QMessageBox messageBox;
          messageBox.critical(0, "Error", "The port should be a number!");
          return;
        }
    }
  }else if(view->ui->btnStartServer->text() == "Stop server"){
    delete server;
    server = nullptr;
    view->ui->btnStartServer->setText("Start server");
    view->ui->txtPortNumber->setEnabled(true);
    view->ui->chckDebugEnable->setEnabled(true);
  }

}

void ControlPanel::on_btnPaneCalibration_released(){
    recalibratePane = true;
}

void ControlPanel::on_btnTargetSetting_released(){
    aruco_gen = true;
}

void ControlPanel::on_btnRefreshIds_released(){
  if(md != nullptr){
    md->setRefresh();
  }
}

void ControlPanel::on_btnUpdatePane_relased(){
    writeToConsole("Updating pane distances...");
    updatePaneSettings();
    writeToConsole("Done.");
}

/*********************************************************************************
 * Mqtt slots
 ************/

void ControlPanel::on_btnMqttConnect_released(){
  if(view->ui->btnMqttConnect->text() == "Connect"){
    mqttClient = new MqttClient(view->ui->txtMqttHost->text(), view->ui->txtMqttPort->text().toInt(), this, view->ui->chckMqttDebugEnable->isChecked());
    mqttClient->connectToHost();
    test->setMqttClient(mqttClient);
  }else{
    mqttClient->disconnect();
    test->setMqttClient(nullptr);
  }

}

void ControlPanel::on_btnMqttSubscribe_released(){
  if(mqttClient){
    mqttClient->subscribeTo(view->ui->txtMqttTopic->text());
  }else{
    writeToConsole(QString("First connect to a host."), red);
  }
}

void ControlPanel::on_chckMqttDebugEnable_toggled(bool checked){
  if(mqttClient)
    mqttClient->setDebugMode(checked);
}

void ControlPanel::on_btnTestRL_released(){
  writeToConsole("Calculating R and L...");
  auto robotId=view->ui->robotIDIn->value();
  auto endPointId=view->ui->targetIDIn->value();
  //md->getCOGCoordinatesOf(30,endPoint);
  //Send R and L params to the robot
  /*if(mqttClient->getRobot(robotId)==nullptr){
      writeToConsole(QString("No ID found."), red);
  }else{*/
       //mqttClient->getRobot(robotId)->goCurve(res.first,res.second);
      //mqttClient->getRobot(30)->goCurve(1,1);
      auto res = calculateR_L(robotId, endPointId);
      writeToConsole(QString("R: ").append(QString::number(res.first)));
      writeToConsole(QString("L: ").append(QString::number(res.second)));

      writeToConsole(QString("600 pixel m-ben: ").append(QString::number(convertPixelsToMeters(600))));

  //}
  //auto res = calculateR_L(robotId, cv::Point2d(2,2));

  auto regId=view->ui->regIDIn->value();
  if (view->ui->robotOK->isChecked()){
      if(mqttClient->getRobot(regId)==nullptr){
           writeToConsole("Failed to send R and L to robot...", red );
      }else{
           //mqttClient->getRobot(robotId)->goCurve(res.first,res.second);
          mqttClient->getRobot(regId)->goCurve(res.first,res.second);
      }
  }
  else{
      writeToConsole(QString("Robot is not available!"));
  }

  //mqttClient->getRobot(robotId)->goCurve(res.first,res.second);
/*
  //Send R and L params to the robot
  if(mqttClient->getRobot(30)==nullptr){
       writeToConsole("Failed to send R and L to robot...", red );
  }else{
       //mqttClient->getRobot(robotId)->goCurve(res.first,res.second);
      mqttClient->getRobot(30)->goCurve(1,1);
  }*/
}

/***************************************************************/

void ControlPanel::writeToConsole(QString text, COLOR color){
  view->writeToConsole(text, color_map[color]);
}

QString ControlPanel::getCalibrationFilePath(){
  return calib_filename;
}

bool ControlPanel::isStreamingOn(){
  return isStreamOn;
}

bool ControlPanel::calibratePane(MarkerDetector *md){
    vector<Point2f> corners;
    vector<Point2f> pane_corners;

    if(md->getCornersOf(1, corners)){
        pane_corners.push_back(corners[0]);
        corners.clear();
        if(md->getCornersOf(2, corners)){
            pane_corners.push_back(corners[0]);
            corners.clear();
            if(md->getCornersOf(3, corners)){
                pane_corners.push_back(corners[0]);
                corners.clear();
                if(md->getCornersOf(4, corners)){
                    pane_corners.push_back(corners[0]);
                    paneCorners = pane_corners;
                    recalibratePane = false;
                    
                    return true;
                }
            }
        }
    }

    return false;
}

void ControlPanel::drawPath(Mat& frame){
//  Vec3b color(0,75,255);
  for(auto coord : markerPath){
      circle(frame,coord, 3, Scalar(PATH_COLOR), 8);

  }
}

void ControlPanel::setEnableOrDisableMqttConnectionFields(bool isEnable){
  if(isEnable){
    view->ui->txtMqttPort->setEnabled(true);
    view->ui->txtMqttHost->setEnabled(true);
    view->ui->btnMqttConnect->setText("Connect");
  }else{
    view->ui->txtMqttPort->setEnabled(false);
    view->ui->txtMqttHost->setEnabled(false);
    view->ui->btnMqttConnect->setText("Disconnect");
  }
}
std::pair<float,float> ControlPanel::calculateR_L(int markerId, int endPointId){
    std::pair<float,float> P;
    float R = 0;
    float L = 0;
    vector<Point2f> robotCorners;
    vector<Point2f> destinationCorners;

    if(md->getCornersOf(markerId,robotCorners) && md->getCornersOf(endPointId,destinationCorners)){
        Point2f centerPoint;
        Point2f endPoint;
        Point2f halfPoint;
        Point2f halfPerp;

        Point2f origo;
        //auto dist = 0;
        auto temp = 0;
        auto pi = CV_PI;


        // marker direction: centerPoint->markerSideHalfPoint

        auto markerSize = cv::norm(cv::Mat(transformCoordinates(robotCorners[0])),cv::Mat(transformCoordinates(robotCorners[1])));
        auto markerSideHalfPoint = Point2f((transformCoordinates(robotCorners[0]).x+transformCoordinates(robotCorners[1]).x)/2,(transformCoordinates(robotCorners[0]).y+transformCoordinates(robotCorners[1]).y)/2);

        //centerPoint=Point2f((transformCoordinates(robotCorners[0]).x+transformCoordinates(robotCorners[1]).x)/2,(transformCoordinates(robotCorners[1]).y+transformCoordinates(robotCorners[2]).y)/2);
        //md->getCOGCoordinatesOf(markerId,centerPoint);
        md->getCenterOf(markerId,centerPoint);
        centerPoint=Point2f(transformCoordinates(centerPoint).x,transformCoordinates(centerPoint).y);

        //endPoint=Point2f((transformCoordinates(destinationCorners[0]).x+transformCoordinates(destinationCorners[1]).x)/2,(transformCoordinates(destinationCorners[1]).y+transformCoordinates(destinationCorners[2]).y)/2);

        md->getCOGCoordinatesOf(endPointId,endPoint);
        endPoint=Point2f(transformCoordinates(endPoint).x,transformCoordinates(endPoint).y);


        //(centerPointx,centerPointy) (markerSideHalfPointx,markerSideHalfPointy)
                //(- (endPointx-halfPointx), endPointy-halfPointy)

        auto markerPerp = Point2f((transformCoordinates(robotCorners[1]).x+transformCoordinates(robotCorners[2]).x)/2,(transformCoordinates(robotCorners[1]).y+transformCoordinates(robotCorners[2]).y)/2);

        //(halfPointx,halfPointy) (endPointx,endPointy)
                //(- (endPointx-halfPointx), endPointy-halfPointy)
        halfPoint=Point2f((centerPoint.x+endPoint.x)/2,(centerPoint.y+endPoint.y)/2);

        //(x,y) (x2,y2)
        //(- (x2-x), y2-y)
        if (centerPoint.x > endPoint.x && centerPoint.y > endPoint.y){
            halfPerp = Point2f(halfPoint.y,halfPoint.x);
            //halfPerp=Point2f((endPoint.x-halfPoint.x), -(endPoint.y-halfPoint.y));
        }
        if (centerPoint.x < endPoint.x && centerPoint.y < endPoint.y){
            halfPerp = Point2f(halfPoint.y,halfPoint.x);
            //halfPerp=Point2f((endPoint.x-halfPoint.x), -(endPoint.y-halfPoint.y));
        }
        if (centerPoint.x > endPoint.x && centerPoint.y < endPoint.y){
            //halfPerp = Point2f(halfPoint.y,halfPoint.x);
            halfPerp=Point2f((endPoint.x-halfPoint.x), -(endPoint.y-halfPoint.y));
        }
        if (centerPoint.x < endPoint.x && centerPoint.y > endPoint.y){
            //halfPerp = Point2f(halfPoint.y,halfPoint.x);
            halfPerp=Point2f((endPoint.x-halfPoint.x), -(endPoint.y-halfPoint.y));
        }

        intersection(centerPoint,markerPerp,halfPoint,halfPerp,origo);

        auto position1 = ((markerSideHalfPoint.x - centerPoint.x)*(endPoint.y - centerPoint.y) - (markerSideHalfPoint.y - centerPoint.y)*(endPoint.x - centerPoint.x)) > 0;
        auto position2 = ((transformCoordinates(robotCorners[2]).x - transformCoordinates(robotCorners[3]).x)*(endPoint.y - transformCoordinates(robotCorners[3]).y) - (transformCoordinates(robotCorners[2]).y - transformCoordinates(robotCorners[3]).y)*(endPoint.x - transformCoordinates(robotCorners[3]).x)) > 0;

        R = cv::norm(cv::Mat(centerPoint),cv::Mat(origo));


        // Calculate angle between the two radius
        auto centerPointNorm = Point2f(centerPoint.x-origo.x,centerPoint.y-origo.y);
        auto endPointNorm = Point2f(endPoint.x-origo.x,endPoint.y-origo.y);
       //uto dot = centerPointNorm.x*endPointNorm.x + centerPointNorm.y*endPointNorm.y;
       //uto det = centerPointNorm.x*endPointNorm.x - centerPointNorm.y*endPointNorm.y;
        //auto angleRad = atan2(centerPointNorm.y - endPointNorm.y, centerPointNorm.x - endPointNorm.x);

        auto angleRad = atan2(centerPointNorm.y, centerPointNorm.x) - atan2(endPointNorm.y, endPointNorm.x);
        if (angleRad < 0){
            angleRad += 2 * pi;
        }
        writeToConsole(QString("cpnx: ").append(QString::number(centerPointNorm.x)));
        writeToConsole(QString("epnx: ").append(QString::number(endPointNorm.x)));

        writeToConsole(QString("cpny: ").append(QString::number(centerPointNorm.y)));
        writeToConsole(QString("epny: ").append(QString::number(endPointNorm.y)));

        writeToConsole(QString("AngleRad: ").append(QString::number(angleRad)));

        //if (angleRad)
        auto angleDeg = (angleRad*180/pi);
        writeToConsole(QString("Alfa: ").append(QString::number(angleDeg)));

        //auto angleDeg = (angleRad/pi*180) + (angleRad > 0 ? 0 : 360);

        double Alfa = angleDeg;
        if (Alfa > 180){
            Alfa = 360 - Alfa;
        }

        // Calculate arc length by circle radius (R) and the bezárt szög (Alfa)
        L = (2*pi*R) * (Alfa/360);
        //L = angleRad * R;

        c_org = origo;
        c_rad = R;

        R=convertPixelsToMeters(R);
        L=convertPixelsToMeters(L);

        /*
        if (markerSideHalfPoint.x > endPoint.x && markerSideHalfPoint.y > endPoint.y){

        }
        if (markerSideHalfPoint.x < endPoint.x && markerSideHalfPoint.y < endPoint.y){
            R = -R;
        }
        if (markerSideHalfPoint.x > endPoint.x && markerSideHalfPoint.y < endPoint.y){
            R = -R;
        }
        if (markerSideHalfPoint.x < endPoint.x && markerSideHalfPoint.y > endPoint.y){

        }*/

       if (!position1){
            R = -R;
        }

        if (position2){
            L = -L;
        }






        //R=origo.x;
        //L=Alfa;

        //R=centerPoint.x;
        //L=centerPoint.y;

        //R=convertPixelsToMeters(origo.x);
        //R=convertPixelsToMeters(origo.y);

        o1 = centerPoint;
        p1 = markerPerp;
        o2 = halfPoint;
        p2 = halfPerp;

        //void line(Mat& , Point halfPoint, Point halfPerp, const Scalar& color, int thickness=1, int lineType=8, int shift=0);
    }

    return std::make_pair(R,L);
}

void ControlPanel::updatePaneSettings(){
    bool isSuccess = false;
    //Read pane corners distances from the gui
    float width = view->ui->txtWidth->text().toFloat(&isSuccess);
    if(isSuccess){
        paneWidthDistanceInMeter = width;
    }else{
        writeToConsole("Invalid pane width. Using default: 1", ERROR);
        paneWidthDistanceInMeter = 1.0;
    }
    float height = view->ui->txtHeight->text().toFloat(&isSuccess);
    if(isSuccess){
        paneHeightDistanceInMeter = height;
    }else{
        writeToConsole("Invalid pane height. Using default: 1", ERROR);
        paneHeightDistanceInMeter = 1.0;
    }
}

float ControlPanel::convertPixelsToMeters(uint pixels){
    int maxWidthInPixel=599;
    int maxWidthInMeter=getPaneWidthInMeters();
    float ratio = (float) maxWidthInMeter/(float)maxWidthInPixel;
    writeToConsole(QString::fromStdString("Pixel méterben: " + to_string(pixels*ratio)), blue);
    return pixels*ratio;
}

Point2f ControlPanel::convertPointToMeters(Point2f point){
    int maxWidthInPixel=599;
    int maxWidthInMeter=getPaneWidthInMeters();
    float ratio = (float) maxWidthInMeter/(float)maxWidthInPixel;
    Point2f result=Point2f(point.x*ratio,point.y*ratio);
    writeToConsole(QString::fromStdString("Pont méterben: "+ to_string(result.x) +" "+ to_string(result.y)), blue);
    return result;
}

unsigned ControlPanel::getPaneWidthInMeters(){
    return paneWidthDistanceInMeter;
}

unsigned ControlPanel::getPaneHeightInMeters(){
    return paneHeightDistanceInMeter;
}



/*void ControlPanel::onMouse(int event, int x, int y, int flags, void* param)
{
    //Mat &frame = *((Mat*)param); //cast and deref the param

    if (event == EVENT_LBUTTONDOWN)
    {

        std::vector<cv::Point>* ptPtr;
        if(ptPtr->size==3){
            ptPtr->clear;
        }
        ptPtr = (std::vector<cv::Point>*)param;
        ptPtr->push_back(cv::Point(x,y));

    }
}

void ControlPanel::drawRectangle(vector<Point> points, Mat& frame){
    for (auto it = points.begin(); it != points.end(); ++it){
        circle(frame, *it, 5, cv::Scalar(0, 0, 255), -1, cv::LINE_8, 0);
    }
    Point middlepoint= Point((int)(points[0].x+points[2].x)/2, (int)(points[0].y+points[2].y)/2);
    //writeToConsole(QString::fromStdString(to_string(middlepoint.x) + to_string(middlepoint.y)));
    circle(frame, middlepoint, 5, cv::Scalar(0, 0, 255), -1, cv::LINE_8, 0);
    Point fourth = Point((int)(middlepoint.x+(middlepoint.x-points[1].x)), (int)(middlepoint.y+(middlepoint.y-points[1].y)));
    circle(frame, fourth, 5, cv::Scalar(0, 0, 255), -1, cv::LINE_8, 0);
    Point vector = Point (points[0].x-points[1].x, points[0].y-points[1].y);

}*/

vector<Point2f> ControlPanel::findTarget(MarkerDetector *md){
    vector<Point2f> corners;
    vector<Point2f> targets;
    vector<Point2f> transformedCorners;
    Mat invM=imgT->M;

    //int targetIds[] ={5, 12};
    //for(int k=0; k<1; k++){
        if(md->getCornersOf(11, corners) && calibrated)
        {
            for(int i =0; i<=3; i++)
            {
                transformedCorners.push_back(Point2f(
                     (corners[i].x * invM.at<double>(0,0) + corners[i].y * invM.at<double>(0,1) + invM.at<double>(0,2))/
                     (corners[i].x * invM.at<double>(2,0) + corners[i].y * invM.at<double>(2,1) + invM.at<double>(2,2)),
                     (corners[i].x * invM.at<double>(1,0) + corners[i].y * invM.at<double>(1,1) + 1 * invM.at<double>(1,2))/
                     (corners[i].x * invM.at<double>(2,0) + corners[i].y * invM.at<double>(2,1) + invM.at<double>(2,2))
                ));
            }
            //writeToConsole(QString::fromStdString("Pont pixelben: "+to_string(transformedCorners[0].x)+" "+to_string(transformedCorners[0].y)));
            /*writeToConsole(QString::fromStdString("Id: "+to_string(5)));
            convertPointToMeters(transformedCorners[0]);
            convertPointToMeters(transformedCorners[1]);
            convertPointToMeters(transformedCorners[2]);
            convertPointToMeters(transformedCorners[3]);*/

            Point center = Point((int)(transformedCorners[0].x+transformedCorners[2].x)/2, (int)(transformedCorners[0].y+transformedCorners[2].y)/2);
            //writeToConsole(QString::fromStdString("28-as közepe: "+to_string(center.x)+" "+to_string(center.y)));
            targets.push_back(Point2f(center));
            Point2f coord, transformedCoord;

            md->getCenterOf(11, coord);
            //writeToConsole(QString::fromStdString("11es közepe az eredetin: "+to_string(5)));
            writeToConsole(QString::fromStdString("11es közepe az eredetin: "+to_string(coord.x)+" "+to_string(coord.y)));
            transformedCoord=Point2f(
                 (coord.x * invM.at<double>(0,0) + coord.y * invM.at<double>(0,1) + invM.at<double>(0,2))/
                 (coord.x * invM.at<double>(2,0) + coord.y * invM.at<double>(2,1) + invM.at<double>(2,2)),
                 (coord.x * invM.at<double>(1,0) + coord.y * invM.at<double>(1,1) + 1 * invM.at<double>(1,2))/
                 (coord.x * invM.at<double>(2,0) + coord.y * invM.at<double>(2,1) + invM.at<double>(2,2))
            );
            writeToConsole(QString::fromStdString("11es közepe a transzformálton: "+to_string(transformedCoord.x)+" "+to_string(transformedCoord.y)));
            writeToConsole(QString::fromStdString("11es közepe méterben: "));
            convertPointToMeters(transformedCoord);

            md->getCenterOf(12, coord);
            writeToConsole(QString::fromStdString("12es közepe az eredetin: "+to_string(coord.x)+" "+to_string(coord.y)));
            transformedCoord=Point2f(
                 (coord.x * invM.at<double>(0,0) + coord.y * invM.at<double>(0,1) + invM.at<double>(0,2))/
                 (coord.x * invM.at<double>(2,0) + coord.y * invM.at<double>(2,1) + invM.at<double>(2,2)),
                 (coord.x * invM.at<double>(1,0) + coord.y * invM.at<double>(1,1) + 1 * invM.at<double>(1,2))/
                 (coord.x * invM.at<double>(2,0) + coord.y * invM.at<double>(2,1) + invM.at<double>(2,2))
            );
            writeToConsole(QString::fromStdString("12es közepe a transzformálton: "+to_string(transformedCoord.x)+" "+to_string(transformedCoord.y)));
            writeToConsole(QString::fromStdString("12es közepe méterben: "));
            convertPointToMeters(transformedCoord);
        }
    //}
    writeToConsole(QString::fromStdString("Target"+ to_string(5)+ ": "+to_string(targets[0].x)+" "+to_string(targets[0].y)));
    //writeToConsole(QString::fromStdString("Target"+ to_string(30)+ ": "+to_string(targets[1].x)+" "+to_string(targets[1].y)));

    return targets;
}

Point2f ControlPanel::transformCoordinates(Point2f point){
    Point2f transformedPoint;
    Mat invM=imgT->M;

    transformedPoint=Point2f(
    (point.x * invM.at<double>(0,0) + point.y * invM.at<double>(0,1) + invM.at<double>(0,2))/
    (point.x * invM.at<double>(2,0) + point.y * invM.at<double>(2,1) + invM.at<double>(2,2)),
    (point.x * invM.at<double>(1,0) + point.y * invM.at<double>(1,1) + 1 * invM.at<double>(1,2))/
    (point.x * invM.at<double>(2,0) + point.y * invM.at<double>(2,1) + invM.at<double>(2,2)));

    return transformedPoint;
}

bool ControlPanel::intersection(Point2f o1, Point2f p1, Point2f o2, Point2f p2, Point2f &r){
    Point2f x = o2 - o1;
    Point2f d1 = p1 - o1;
    Point2f d2 = p2 - o2;

    float cross = d1.x*d2.y - d1.y*d2.x;
    if (abs(cross) < /*EPS*/1e-8)
        return false;

    double t1 = (x.x * d2.y - x.y * d2.x)/cross;
    r = o1 + d1 * t1;
    return true;
}

// vonal rajzolo fgv frame es 2 pont alapjan
void ControlPanel::drawLine(Mat& frame, cv::Point2f o1, cv::Point2f p1){
    //cv::line(Mat& vertical, Point(o1), Point(p1), const Scalar& color, int thickness=1, int lineType=8, int shift=0);
    cv::line(frame, o1, p1, Scalar(0, 255, 255), 3);
    //futtasd
}
