#include "inc/robot.h"

Robot::Robot(unsigned id, QString topic, MqttClient *mqtt)
  : id(id)
  , topic(topic)
  , mqtt(mqtt){}

Robot::~Robot(){}

unsigned Robot::getId()
{
  return id;
}

QString Robot::getTopic()
{
  return topic;
}

bool Robot::sendMessage(QString msg, QString subtopic){
  if(subtopic.size() > 0)
    return mqtt->sendMessage(this->topic + QString("/").append(subtopic), msg);
  else
    return mqtt->sendMessage(this->topic, msg);
}

void Robot::goCurve(float R, float L){
  QJsonObject commandObj;
  commandObj.insert("R", QJsonValue(QString::number(R)));
  commandObj.insert("L", QJsonValue(QString::number(L)));
  //commandObj.insert("R", QJsonValue(R));
  //commandObj.insert("L", QJsonValue(L));

  QJsonDocument doc(commandObj);
  this->sendMessage(doc.toJson(QJsonDocument::Compact), CURVE);
}

void Robot::odoreset(int x, int y, float theta){
  QJsonObject commandObj;
  commandObj.insert("X", QJsonValue(QString::number(x)));
  commandObj.insert("Y", QJsonValue(QString::number(y)));
  commandObj.insert("Theta", QJsonValue(QString::number(theta)));

  QJsonDocument doc(commandObj);
  this->sendMessage(doc.toJson(), ODORESET);
}
