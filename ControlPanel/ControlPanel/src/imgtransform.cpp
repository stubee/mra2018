#include "inc/imgtransform.h"
#include "inc/controlpanel.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;
using namespace std;

ImgTransform::ImgTransform(ControlPanel* controller) : controller(controller){}

cv::Mat ImgTransform::getVerticalView(const std::vector<Point2f>& pts, const cv::Mat& frame){
  //order points
  vector<int> tmp_sum;
  vector<int> tmp_diff;
  for(const auto pt : pts){
    tmp_sum.push_back(pt.x + pt.y);
    tmp_diff.push_back(pt.x - pt.y);
  }
  /*int br_ind = distance(tmp_sum.begin(), max_element(tmp_sum.begin(), tmp_sum.end())); // right bottom
  int tl_ind = distance(tmp_sum.begin(), min_element(tmp_sum.begin(), tmp_sum.end())); // left top
  int tr_ind = distance(tmp_diff.begin(), min_element(tmp_diff.begin(), tmp_diff.end())); // right top
  int bl_ind = distance(tmp_diff.begin(), max_element(tmp_diff.begin(), tmp_diff.end())); // left bottom

  auto widthA = sqrt( ( pow((pts[br_ind].x - pts[bl_ind].x),2) ) + ( pow( (pts[br_ind].y - pts[bl_ind].y ),2 ) ) ); // bottom length of the marker
  auto widthB = sqrt( ( pow((pts[tr_ind].x - pts[tl_ind].x),2) ) + ( pow( (pts[tr_ind].y - pts[tl_ind].y ),2 ) ) ); // top length of the marker

  auto heightA = sqrt( ( pow((pts[tr_ind].x - pts[br_ind].x),2) ) + ( pow( (pts[tr_ind].y - pts[br_ind].y ),2 ) ) );
  auto heightB = sqrt( ( pow((pts[tl_ind].x - pts[bl_ind].x),2) ) + ( pow( (pts[tl_ind].y - pts[bl_ind].y ),2 ) ) );
    */
  //auto maxWidth = max(int(widthA), int(widthB));
  int maxWidth = 600;
  auto maxHeight = (int)((float)maxWidth * ((float)controller->getPaneHeightInMeters() / (float)controller->getPaneWidthInMeters()));

  //int x = max(maxWidth, maxHeight);
  vector<Point2f> dst= {
    Point2f(0, 0),
    Point2f(maxWidth - 1, 0),
    //Point2f(x, 0),
    Point2f(maxWidth - 1, maxHeight - 1),
    //Point2f(x, x),
    Point2f(0, maxHeight - 1),
    //Point2f(0, x),
  };



  M = getPerspectiveTransform(pts, dst);
  Mat warped;
  warpPerspective(frame, warped, M,  Size(maxWidth, maxHeight));
  //warpPerspective(frame, warped, M,  Size(x,x));
 // warpPerspective(frame, warped, M,  Size(800, 600));
  //warpPerspective(frame, warped, M,  Size(frame.size[1], frame.size[0]));
  return warped;
}
