#include "inc/socketapi.h"
#include <QtWebSockets/qwebsocketserver.h>
#include <QtWebSockets/qwebsocket.h>
#include <QDebug>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>

QT_USE_NAMESPACE

SocketAPI::SocketAPI(quint16 port, ControlPanel *controller, bool debug, QObject *parent) :
  QObject(parent),
  m_pWebSocketServer(new QWebSocketServer(QStringLiteral("API Server"),
                                          QWebSocketServer::NonSecureMode, this)),
  m_debug(debug),
  controller(controller)
{
  if (m_pWebSocketServer->listen(QHostAddress::Any, port)) {
      if (m_debug)
          qDebug() << "API server listening on port" << port;
      connect(m_pWebSocketServer, &QWebSocketServer::newConnection,
              this, &SocketAPI::onNewConnection);
      connect(m_pWebSocketServer, &QWebSocketServer::closed, this, &SocketAPI::closed);
  }
}

SocketAPI::~SocketAPI()
{
    m_pWebSocketServer->close();
    qDeleteAll(m_clients.begin(), m_clients.end());
}

void SocketAPI::onNewConnection()
{
    QWebSocket *pSocket = m_pWebSocketServer->nextPendingConnection();

    connect(pSocket, &QWebSocket::textMessageReceived, this, &SocketAPI::processTextMessage);
    connect(pSocket, &QWebSocket::binaryMessageReceived, this, &SocketAPI::processBinaryMessage);
    connect(pSocket, &QWebSocket::disconnected, this, &SocketAPI::socketDisconnected);

    m_clients << pSocket;
}

void SocketAPI::processTextMessage(QString message)
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (m_debug)
        qDebug() << "Message received:" << message;
    if (pClient) {

      QJsonObject received_cmd = ObjectFromString(message);

      if(received_cmd.empty()){
        pClient->sendTextMessage("Error occurred with processing JSON string.");
        return;
      }

      if(received_cmd.contains("cmd")){
        QJsonObject response;

        if(received_cmd["cmd"] == "clients")
          {
            response.insert("clients", QJsonValue(m_clients.size()));
            pClient->sendTextMessage(QJsonDocument(response).toJson());
          }
        else if(received_cmd["cmd"] == "get_corners")
          {
            if(received_cmd.contains("id"))
              {
                QJsonArray corners;
                QJsonObject coord1;
                QJsonObject coord2;
                QJsonObject coord3;
                QJsonObject coord4;
                coord1.insert("x", QJsonValue(2));
                coord1.insert("y", QJsonValue(1));
                coord2.insert("x", QJsonValue(6));
                coord2.insert("y", QJsonValue(3));
                coord3.insert("x", QJsonValue(4));
                coord3.insert("y", QJsonValue(4));
                coord4.insert("x", QJsonValue(6));
                coord4.insert("y", QJsonValue(9));

                corners.append(coord1);
                corners.append(coord2);
                corners.append(coord3);
                corners.append(coord4);

                response.insert("corners", corners);
                pClient->sendTextMessage(QJsonDocument(response).toJson());
              }
          }

      }else{
        pClient->sendTextMessage("The command should have cmd key.");
      }



     //pClient->sendTextMessage(message);
    }
}

void SocketAPI::processBinaryMessage(QByteArray message)
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (m_debug)
        qDebug() << "Binary Message received:" << message;
    if (pClient) {
        pClient->sendBinaryMessage(message);
    }
}

void SocketAPI::socketDisconnected()
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (m_debug)
        qDebug() << "socketDisconnected:" << pClient;
    if (pClient) {
        m_clients.removeAll(pClient);
        pClient->deleteLater();
    }
}

QJsonObject SocketAPI::ObjectFromString(const QString& in)
{
    QJsonObject obj;

    QJsonDocument doc = QJsonDocument::fromJson(in.toUtf8());

    // check validity of the document
    if(!doc.isNull())
    {
        if(doc.isObject())
        {
            obj = doc.object();
        }
        else
        {
            qDebug() << "Document is not an object" << endl;
        }
    }
    else
    {
        qDebug() << "Invalid JSON...\n" << in << endl;
    }

    return obj;
}
