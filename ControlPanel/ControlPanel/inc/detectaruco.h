#ifndef DETECTARUCO_H
#define DETECTARUCO_H

#include <opencv2/aruco.hpp>
#include "inc/imgtransform.h"
#include "inc/mainwindow.h"

class MainWindow;

class MarkerDetector{
private:
  std::string camera_parameters_filename;
  int dictId;
  float marker_size;
  std::string dictionary_filename;
  cv::Mat camMatrix;
  cv::Mat distCoeffs;
  cv::Ptr<cv::aruco::Dictionary> dictionary;
  cv::aruco::Dictionary dict3x3;
  cv::Ptr<cv::aruco::DetectorParameters> detectorParams;
  std::vector< cv::Point2f > imagePoints;


  std::vector< int > ids;
  std::vector< std::vector< cv::Point2f > > corners, rejected, imgPointsVector;
  std::vector< cv::Vec3d > rvecs, tvecs;


  bool refresh = true;

  bool estimatePose;
  ImgTransform* imgT = nullptr;
  MainWindow* gui;

public:
  /**
   * @brief Constructor
   * @param camera_parameters_filename
   * @param dictId
   * @param marker_size
   */
  MarkerDetector(std::string camera_parameters_filename, int dictId, float marker_size, std::string dictionary_filename);
  //std::vector< cv::Point2f > MarkerDetector::getAxisPointsOf(int markerId, std::vector< cv::Point2f >& imagePoints);

  /**
   * @brief Detects marker on the given frame.
   * @param frame [in]
   * @return
   */
  cv::Mat detectMarker(cv::Mat frame);

  /**
   * @brief setImageTransformator
   * @param imgT
   */
  void setImageTransformator(ImgTransform* imgT);

  /**
   * @brief setGui
   * @param gui
   */
  void setGui(MainWindow *gui);

  /**
   * @brief Returns with a vector of detected aruco ids.
   * @return
   */
  std::vector < int > getIds();

  /**
   * @brief Returns with a vector which contains the detected markers corner positions.
   * @return
   */
  std::vector< std::vector< cv::Point2f > > getCorners();

  bool getRVecsOf(int markerId, cv::Vec3d& rvecs);

  bool getTVecsOf(int markerId, cv::Vec3d& tvecs);

  /**
   * @brief Gives back the corner coordinates of the given marker's id
   * @param markerId [in]
   * @param corners [out]
   * @return bool True if the given ID detected, false otherwise.
   */
  bool getCornersOf(int markerId, std::vector< cv::Point2f >& corners);

  /**
   * @brief Gives back the coordinates of center of gravity of the given marker.
   * @param makerId [in] the given marker
   * @param coord [out] COG coordinates of the given marker
   * @return True if marker was detected, false otherwise.
   */
  bool getCOGCoordinatesOf(int makerId, cv::Point2f& coord);

  bool getCenterOf(int markerId, cv::Point2f& center);

  std::vector<cv::Point3f> getCornersInCameraWorld(double side, cv::Vec3d rvec, cv::Vec3d tvec);
  std::vector<cv::Point3f> getCornersOfInCameraWorld(double side, int markerId);

  cv::Mat getCameraMatrix();
  cv::Mat getDistCoeffs();

  void setRefresh();
private:

  /**
   * @brief Reads the calibrated camera file and store into camMatrix and distCoeffs.
   * @param filename [in]
   * @param camMatrix [out]
   * @param distCoeffs [out]
   * @return
   */
  static bool readCameraParameters(std::string filename, cv::Mat &camMatrix, cv::Mat &distCoeffs);


};

#endif // DETECTARUCO_H
