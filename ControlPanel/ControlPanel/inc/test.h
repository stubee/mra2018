#ifndef TEST_H
#define TEST_H

#include <QObject>

#include "controlpanel.h"
#include "mqttapi.h"

class MqttClient;

class Test : public QObject{

  Q_OBJECT

  ControlPanel* controller;
  MqttClient* mqtt;

public:
  Test(ControlPanel* controller, MqttClient* mqtt, QObject *parent);

  void setMqttClient(MqttClient* mqtt);

private:

  void loadAvailableRobots();
  void sendCurveControl();

private slots:

  void on_btnRefreshRobots_release();
  void on_btnSendCurveControl_release();
  void on_btnSendCustomText_release();
};



#endif // TEST_H
