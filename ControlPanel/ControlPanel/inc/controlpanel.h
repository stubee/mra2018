﻿#ifndef CONTROLPANEL_H
#define CONTROLPANEL_H

#include <QObject>
#include "mainwindow.h"
#include "socketapi.h"
#include "detectaruco.h"
#include <opencv2/opencv.hpp>
#include <vector>
#include <deque>
#include "mqttapi.h"
#include "test.h"

class SocketAPI;
class MqttClient;

class ControlPanel : public QObject
{
  Q_OBJECT

public:

  enum COLOR{
    black,
    blue,
    red,
    orange,
    darkblue
  };

private:
  MainWindow *view;
  ImgTransform *imgT;
  MarkerDetector *md;

  QString calib_filename;
  QString dictionary_filename;
  SocketAPI *server;
  MqttClient *mqttClient;

  Test *test;

  bool detectMarkers = true;
  bool isStreamOn = false;
  bool recalibratePane = true;
  bool aruco_gen = false;
  bool calibrated = false;

  std::vector<cv::Point2f> paneCorners;
  float paneHeightDistanceInMeter = 0.0; //from the GUI, in meters
  float paneWidthDistanceInMeter = 0.0; //from the GUI, in meters
  
  #define PATH_LENGTH 50
  #define PATH_COLOR 255,0,0

  std::deque<cv::Point2f> markerPath;
  std::map<COLOR, QString> color_map =
  {
    std::make_pair( black,    "black"     ),
    std::make_pair( blue,     "blue"      ),
    std::make_pair( red,      "red"       ),
    std::make_pair( orange,   "#ff8c00"   ),
    std::make_pair( darkblue, "#1f5687"   )

  };

  /**
   * @brief startVideoStream Starts the video stream with the selected camera.
   * @param camera_index The selected camera index
   * @param md MarkerDetector object
   */
  void startVideoStream(int camera_index, MarkerDetector* md = nullptr);

  /**
   * @brief calibratePane Saves the 4 markers position of the pane.
   * @param md MarkerDetector object
   * @return
   */
  bool calibratePane(MarkerDetector* md);

  /**
   * @brief drawPath Draws the path of the selected marker.
   * @param frame
   */
  void drawPath(cv::Mat& frame);
  
  std::pair<float,float> calculateR_L(int markerId, int endPointId);
  
  /**
   * @brief Reads and save settings of the pane from the GUI.
   */
  void updatePaneSettings();

public:
  explicit ControlPanel(MainWindow *view, QObject *parent = nullptr);
  ~ControlPanel();

  /**
   * @brief getCalibrationFilePath Gives back the path of the camera calibration file.
   * @return
   */
  QString getCalibrationFilePath();

  /**
   * @brief isStreamingOn
   * @return
   */
  bool isStreamingOn();

  /**
   * @brief writeToConsole Writes out the given text with the given color to the UI console.
   * @param text
   * @param color
   */
  void writeToConsole(QString text, COLOR color = black);

  /**
   * @brief setEnableOrDisableMqttConnectionFields
   * @param isEnable If it is true then allowed to edit connection inputs.
   */
  void setEnableOrDisableMqttConnectionFields(bool isEnable);
  
public:
  
  unsigned getPaneWidthInMeters();
  unsigned getPaneHeightInMeters();
  
  float convertPixelsToMeters(uint pixels);
  cv::Point2f convertPointToMeters(cv::Point2f point);
  bool intersection(cv::Point2f o1, cv::Point2f p1, cv::Point2f o2, cv::Point2f p2, cv::Point2f &r);
  cv::Point2f transformCoordinates(cv::Point2f point);

signals:

private slots:
  void on_btnOpenCamera_released();
  void on_chckArucoDetect_pressed();
  void on_chckShowPerspTrans_pressed();
  void on_btnClearConsole_released();
  void on_cameraSelector_currentIndexChanged(int index);
  void on_btnStartServer_released();
  void on_btnRefreshIds_released();
  void on_btnPaneCalibration_released();
  void on_btnUpdatePane_relased();
  void on_btnTargetSetting_released();
  //static void onMouse(int event, int x, int y, int flags, void* param);
  //void drawRectangle(std::vector<cv::Point> points, cv::Mat& frame);

  void drawLine(cv::Mat& frame, cv::Point2f o1, cv::Point2f p1);
  std::vector<cv::Point2f> findTarget(MarkerDetector *md);

  //Mqtt client
  void on_btnMqttConnect_released();
  void on_btnMqttSubscribe_released();
  void on_chckMqttDebugEnable_toggled(bool checked);

  friend class Test;

  // Test calculation of Radius and Circle L
  void on_btnTestRL_released();




};

#endif // CONTROLPANEL_H
