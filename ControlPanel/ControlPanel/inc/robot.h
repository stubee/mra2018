#ifndef ROBOT_H
#define ROBOT_H

#include <QObject>
#include "mqttapi.h"

class MqttClient;

//define subtopics for control
#define CURVE "curvecontrol"
#define ODORESET "odoreset"

class Robot : public QObject{
  Q_OBJECT
  unsigned id;
  QString topic;
  MqttClient* mqtt;
  
public:
  Robot(unsigned id, QString topic, MqttClient* mqtt);
  ~Robot();

  unsigned getId();
  QString getTopic();

  /**
   * @brief goCurve
   * @param R radius of the circle in meters +-
   * @param L length of the curve in meters +-
   */
  void goCurve(float R, float L);

  /**
   * @brief odoreset
   * @param x X coordinate
   * @param y Y coordinate
   * @param theta roll in radian
   */
  void odoreset(int x, int y, float theta);

  bool sendMessage(QString msg, QString subtopic = "");

};

#endif // ROBOT_H
