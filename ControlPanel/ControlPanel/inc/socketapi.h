#ifndef SOCKETAPI_H
#define SOCKETAPI_H

#include "controlpanel.h"

#include <QObject>
#include <QList>
#include <QJsonObject>
#include <QByteArray>
#include <QtMqtt>

QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QWebSocket)

class ControlPanel;

/**
 * @brief The SocketAPI class
 * Qt websocket class. Client example: ControlPanel/client.html
 * Ez igazából nincs használva, csak ha már megírtam nem volt szívem törölni :D
 */
class SocketAPI : public QObject
{
  Q_OBJECT
public:
  explicit SocketAPI(quint16 port, ControlPanel *controller, bool debug = false, QObject *parent = Q_NULLPTR);
  ~SocketAPI();

Q_SIGNALS:
  void closed();

private Q_SLOTS:
  void onNewConnection();
  void processTextMessage(QString message);
  void processBinaryMessage(QByteArray message);
  void socketDisconnected();
  QJsonObject ObjectFromString(const QString& in);

private:
  QWebSocketServer *m_pWebSocketServer;
  QList<QWebSocket *> m_clients;
  bool m_debug;
  ControlPanel *controller;
};


#endif // SOCKETAPI_H
