#ifndef MQTTAPI_H
#define MQTTAPI_H


#include "controlpanel.h"
#include "robot.h"
#include <QObject>
#include <QtMqtt>
#include <QSet>
#include <QList>

class MqttClient;
class ControlPanel;
class Robot;

#define API_NAME "MQTT"
#define INFO ControlPanel::COLOR::darkblue
#define ERROR ControlPanel::COLOR::red
#define CONTROLLER_TOPIC_NAME "controller"


struct Validator{
  Validator(){}

  bool validateReg(QJsonObject obj, std::vector<QString>& errors);
};




/**
 * @brief The MqttAPI class
 * Based on Mqtt
 */
class MqttClient : public QObject{
  Q_OBJECT
  QString host;
  quint16 port;
  ControlPanel *controller;
  bool debug;
  QMqttClient* client;
  Validator validator;
  QSet<QMqttSubscription*> subscriptions;

  Q_DISABLE_COPY(MqttClient)
  
  QList<Robot*> registeredRobots;

public:
  explicit MqttClient(QString host, quint16 port, ControlPanel *controller, bool debug = false, QObject *parent = Q_NULLPTR);
  ~MqttClient();

  /**
   * @brief setDebugMode If its true then allowed to print debug information to the console.
   * @param isEnabled
   */
  void setDebugMode(bool isEnabled);

  /**
   * @brief connectToHost
   */
  void connectToHost();

  /**
   * @brief disconnect Disconnect from the host
   */
  void disconnect();

  void subscribeTo(QString topic);
  bool sendMessage(QString topic, QString msg);
  
  /**
   * @brief getRobot
   * @param id
   * @return A pointer which points to the robot if there is one with the given id. Nullptr otherwise.
   */
  Robot* getRobot(unsigned id);

  std::vector<int> getRegisteredRobotIds();

private slots:
  void event_messageReceived(const QByteArray &message, const QMqttTopicName &topic);
  void event_stateChanged();
  void event_connected();
  void event_disconnected();

private:
  void messageParser(QString message);
  void commandHandler(QJsonObject& cmd);
  void dumpErrors(std::vector<QString>& errors);

  //Commands
  bool registerRobot(unsigned id, QString topic);
  void deleteRegisteredRobots();
};



#endif // MQTTAPI_H
