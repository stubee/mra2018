#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"
#include <QMainWindow>
#include "detectaruco.h"
#include "imgtransform.h"
#include <QList>
#include <map>

class MarkerDetector;
class Test;

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:


  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

  QPushButton* getBtnOpenCamera();

private:
  Ui::MainWindow *ui;
  QString calib_filename;

  bool detectMarkers = true;

  std::vector<QList<QSize>> cameras_resolution;

  /**
   * @brief loadCameras Fills the combobox with the available cameras
   */
  void loadCameras();

  /**
   * @brief loadResolutions Loads the resolutions of the selected camera.
   * @param resolutions
   */
  void loadResolutions(QList<QSize> resolutions);

  /**
   * @brief getIsCheckedShowPerspective
   * @return
   */
  bool getIsCheckedShowPerspective();

  /**
   * @brief writeToConsole It prints the text to the console.
   * @param text
   * @param color
   */
  void writeToConsole(QString text, QString color);

private slots:


friend class ControlPanel;
friend class MarkerDetector;
friend class Test;

};

#endif // MAINWINDOW_H
