#ifndef IMGTRANSFORM_H
#define IMGTRANSFORM_H

#include <opencv2/opencv.hpp>
#include <vector>
//#include "inc/controlpanel.h"

#define PI 3.1415926

class ControlPanel;

class ImgTransform
{
private:
    ControlPanel* controller;
public:
  ImgTransform(ControlPanel* controller);
  cv::Mat M;
  int x;

  cv::Mat getVerticalView(const std::vector<cv::Point2f>& pts, const cv::Mat& frame);


};

#endif // IMGTRANSFORM_H
