#-------------------------------------------------
#
# Project created by QtCreator 2018-02-21T09:13:36
#
#-------------------------------------------------

QT += core gui multimedia websockets mqtt

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ControlPanel
TEMPLATE = app
CONFIG += c++11

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    src/main.cpp \
    src/mainwindow.cpp \
    src/detectaruco.cpp \
    src/imgtransform.cpp \
    src/controlpanel.cpp \
    src/socketapi.cpp \
    src/mqttapi.cpp \
    src/robot.cpp \
    src/test.cpp

HEADERS += \
    inc/mainwindow.h \
    inc/detectaruco.h \
    inc/imgtransform.h \
    inc/controlpanel.h \
    inc/socketapi.h \
    inc/mqttapi.h \
    inc/robot.h \
    inc/test.h

FORMS += \
    forms/mainwindow.ui


INCLUDEPATH += $$PWD/inc

win32{
INCLUDEPATH += $$(OPENCV_SDK_DIR)/include

LIBS += -L$$(OPENCV_SDK_DIR)/x86/mingw/lib \
        -lopencv_core340        \
        -lopencv_highgui340     \
        -lopencv_imgcodecs340   \
        -lopencv_imgproc340     \
        -lopencv_features2d340  \
        -lopencv_calib3d340     \
        -lopencv_videoio340     \
        -lopencv_aruco340       \
        -lopencv_flann340
}

unix{
OPENCV_SDK_DIR=$$PWD/../../build/opencv/install
INCLUDEPATH += $$OPENCV_SDK_DIR/include
LIBS += -L$$OPENCV_SDK_DIR/lib \
        -lopencv_core        \
        -lopencv_highgui     \
        -lopencv_imgcodecs   \
        -lopencv_imgproc     \
        -lopencv_aruco       \
        -lopencv_features2d  \
        -lopencv_calib3d     \
        -lopencv_videoio     \
        -lopencv_flann
}

DISTFILES += \
    forms/img/refresh.png

RESOURCES += \
    img_resources.qrc
