#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/aruco.hpp>

using namespace std;
using namespace cv;

namespace {
    const char* about = "Create a custom dictionary";
    const char* keys  =
            "{@outfile | dict.yml | Output filename }"
            "{n        | 1024   | Number of markers in dictionary }"
            "{d        | 3        | Marker inner dimension }";

}


int main(int argc, char *argv[]) {
  CommandLineParser parser(argc, argv, keys);
  parser.about(about);

  if(argc < 3) {
    parser.printMessage();
    return 0;
  }

  int dimension = parser.get<int>("d");
  int nMarkers = parser.get<int>("n");
  String out = parser.get<String>("@outfile");

  cout << "Dimension: " << dimension << endl;
  cout << "nMarkers: " << nMarkers << endl;
  cout << "Out: " << out << endl;

  if(!parser.check()) {
    parser.printErrors();
    return 0;
  }

  cv::aruco::Dictionary dictionary = cv::aruco::generateCustomDictionary(nMarkers, dimension);
  cv::Mat store = dictionary.bytesList;
  cv::FileStorage fs(out, cv::FileStorage::WRITE);
  fs << "MarkerSize" << dictionary.markerSize;
  fs << "MaxCorrectionBits" << dictionary.maxCorrectionBits;
  fs << "ByteList" << dictionary.bytesList;
  fs.release();

  return 0;
}
