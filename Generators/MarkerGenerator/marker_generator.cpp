#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/aruco.hpp>

using namespace std;
using namespace cv;

namespace {
  const char* about = "Create an ArUco marker image from a dictionary";
  const char* keys  =
          "{d          |        | path to dictionary file}"
          "{id         |        | Marker id in the dictionary }"
          "{ms         | 200    | Marker size in pixels }"
          "{bb         | 1      | Number of bits in marker borders }"
          "{range      | false  | Generate markers between the given range }"
          "{from       |        | If range is true then generating starts from the given id. }"
          "{to         |        | If range is true then generating stops at the given id. }";

}


int main(int argc, char *argv[]) {
  CommandLineParser parser(argc, argv, keys);
  parser.about(about);

  if(argc < 3) {
    parser.printMessage();
    return 0;
  }

  std::string dictionaryPath = parser.get<std::string>("d");
  int markerId = parser.get<int>("id");
  int markerSize = parser.get<int>("ms");
  bool range = parser.get<bool>("range");
  int from = 0;
  int to = 0;
  if(range){
    from = parser.get<int>("from");
    to = parser.get<int>("to");
  }


  if(!parser.check()) {
    parser.printErrors();
    return 0;
  }

  cv::FileStorage fsr(dictionaryPath, cv::FileStorage::READ);
  int mSize, mCBits;
  cv::Mat bits;
  fsr["MarkerSize"] >> mSize;
  fsr["MaxCorrectionBits"] >> mCBits;
  fsr["ByteList"] >> bits;
  fsr.release();
  cv::aruco::Dictionary dictionary = cv::aruco::Dictionary(bits, mSize, mCBits);
  cv::Ptr<cv::aruco::Dictionary> dict = &dictionary;
  cv::Mat markerImage;
  cv::Mat markerImage2;
  std::string filename = "";
  //save to file marker
  if(range)
  {
    for(int id = from; id < to; id++)
    {
      cv::aruco::drawMarker(dict, id, markerSize, markerImage, 1);
      filename = string("marker_").append(std::to_string(id)).append(".png");
      imwrite(filename, markerImage);
      markerImage.release();
    }
  }else{
    cv::aruco::drawMarker(dict, markerId, markerSize, markerImage, 1);
    filename = string("marker_").append(std::to_string(markerId)).append(".png");
    imwrite(filename, markerImage);
    markerImage.release();
  }


  return 0;
}
