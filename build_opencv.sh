#!/bin/bash

if [ ! -d "build" ]; then
  echo "Create build directory..."
  mkdir build
  cd build
else
  cd build
fi

if [ ! -d "opencv" ]; then
  echo "Create directory for opencv build..."
  mkdir opencv
  cd opencv
else
  echo "Delete opencv directory..."
  rm -rf opencv
  echo "Create directory for opencv build..."
  mkdir opencv
  cd opencv
fi

echo "Generate build system..."
cmake ../../3rdparty/opencv -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=./install

echo "Add contrib modules..."
cmake -DOPENCV_EXTRA_MODULES_PATH=../../3rdparty/opencv_contrib/modules ../../3rdparty/opencv

echo "Building..."
make -j5 install

