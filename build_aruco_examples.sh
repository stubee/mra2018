#!/bin/bash

if [ ! -d "build" ]; then
  echo "Create build directory..."
  mkdir build
  cd build
else
  cd build
fi

if [ ! -d "aruco" ]; then
  echo "Create directory for aruco build..."
  mkdir aruco
  cd aruco
else
  echo "Delete aruco directory..."
  rm -rf aruco
  echo "Create directory for aruco build..."
  mkdir aruco
  cd aruco
fi

cmake ../../aruco
make -j8
